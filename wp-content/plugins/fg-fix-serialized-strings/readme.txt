=== FG Fix Serialized Strings ===
Contributors: Frédéric GILLES
Plugin Uri: http://wordpress.org/extend/plugins/fg-fix-serialized-strings
Tags: serialized, unserialized, wp_options, fix, widgets
Requires at least: 3.0
Tested up to: WP 3.8.1
Stable tag: 1.1.0
License: GPLv2
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=fred%2egilles%40free%2efr&lc=FR&item_name=Fr%c3%a9d%c3%a9ric%20GILLES&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted

Fix the broken serialized strings in the options table

== Description ==

This plugin fixes the serialized strings in the wp_options table. It recalculate the length of each strings.
It can be useful after a migration if you have done a global search/replace in the SQL dump.

Major features include:

* Repair the wp_options table
* Restore the lost text widgets

== Installation ==

1.  Install the plugin in the Admin => Plugins menu => Add New => Upload => Select the zip file => Install Now
2.  Activate the plugin in the Admin => Plugins menu
3.  Go to the plugin screen: Tools => Fix serialized strings

== Frequently Asked Questions ==

No questions yet.

Don't hesitate to let a comment on the forum or to report bugs if you found some.
http://wordpress.org/support/plugin/fg-fix-serialized-strings

== Screenshots ==

1. Parameters screen

== Translations ==
* English (default)
* French (fr_FR)
* other can be translated

== Changelog ==

= 1.1.0 =
* New: Restore the lost text widgets
* New: Tested with WordPress 3.8.1

= 1.0.0 =
* Initial version

== Upgrade Notice ==

= 1.1.0 =
New: Restore the lost text widgets
New: Tested with WordPress 3.8.1

= 1.0.0 =
Initial version
