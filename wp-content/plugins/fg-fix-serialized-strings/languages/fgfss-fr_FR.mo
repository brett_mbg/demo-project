��          \      �       �       �   &   �   :        L     c  Y   z  C   �  �    +   �  )     Z   <  &   �  &   �  g   �  P   M                                       %d string fixed %d strings fixed %d widget restored %d widgets restored As it will change your database, please do a backup first. Fix Serialized Strings Fix serialized strings Fix the serialized strings from the options table by recalculating all the string lengths The restored widgets will be found in the inactive widgets section. Project-Id-Version: Fix Serialized Strings
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-15 06:51+0100
PO-Revision-Date: 
Last-Translator: F. GILLES
Language-Team: F. GILLES
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n>1;
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: .
 %d chaîne réparée %d chaînes réparées %d widget restauré %d widgets restaurés Comme la base de données va être modifiée, veuillez faire une sauvegarde au préalable. Réparation des chaînes sérialisées Réparation des chaînes sérialisées Répare les chaînes sérialisées de la table options en recalculant toutes les longueurs des chaînes Les widgets restaurés seront trouvés dans la section des widgets désactivés. 